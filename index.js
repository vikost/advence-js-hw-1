'use strict'

// Прототипное наследие дает возможность использовать свойства и методы 
// родительского обьекта в созданых на их основе дочерних обьектах

class Employee{
    constructor(name, age, salary){
        this._name = name
        this._age = age
        this._salary = salary
    }
    set name(newName){
        this._name = newName;
    }
    get name(){
        return this._name
    }
    set age(newAge){
        this._age = newAge;
    }
    get age(){
        return this._age
    }
    set salary(newSalary){
        this._salary = newSalary;
    }
    get salary(){
        return this._salary
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang
    }
    get salary(){
        return this._salary*3
    }
}

const programmer1 = new Programmer('Tom', 23, 3000, ['c++', 'js', 'python']);
const programmer2 = new Programmer('Lucas', 18, 1200, ['c++', 'js']);
const programmer3 = new Programmer('Alex', 26, 8000, ['c++', 'js', 'python', 'c#']);

console.log(programmer1.salary);
console.log(programmer2.salary);
console.log(programmer3.salary);